const express = require('express');
const sessionRoutes = require('./routes/session.routes');
const env = require('./config/env');

const PORT = env.server.port;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api/session', sessionRoutes);

app.listen(PORT);
