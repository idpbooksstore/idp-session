const express = require('express');
const router = express();

const userController = require('../controllers/session.controller');

router.get('/verify/:token', userController.verifyToken);
router.get('/sign/:user', userController.generateToken);

module.exports = router;
