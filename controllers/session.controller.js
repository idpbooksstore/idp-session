const jwt = require('jsonwebtoken');
const secret = require('../config/secret');

exports.verifyToken = (req, res) => {
  const { token } = req.params;

  try {
    const { user } = jwt.verify(token, secret);
    return res.status(200).send({ message: 'Succes', user });
  } catch (err) {
    return res.status(500).send({ message: err, user: '' });
  }
};

exports.generateToken = async (req, res) => {
  const { user } = req.params;

  try {
    const token = jwt.sign({ user }, secret, { expiresIn: '1h' });
    return res.status(200).send({ message: 'Succes', token });
  } catch (err) {
    return res.status(500).send({ message: err, token: '' });
  }
};
