const dotenv = require('dotenv');

dotenv.config();

const env = {
  server: {
    port: +(process.env.PORT ?? 8080),
  },
  nodeEnv: process.env.NODE_ENV ?? 'development',
};

module.exports = env;
